#!/usr/bin/env bash

# Backup Directory
#BD=$PWD/backup/`date +%Y-%m-%d-%H-%M-%S`
BD=$PWD/backup/latest

# Export .env vars
export $(egrep -v '^#' .env | xargs)

# Backup Database
mkdir -p $BD/postgresql
docker-compose exec -u ${POSTGRES_USER:-postgres} db pg_dump ${POSTGRES_DB:-wallet} > $BD/postgresql/${POSTGRES_DB:-wallet}.sql
if [ ! -f $BD/postgresql/${POSTGRES_DB:-wallet}.sql ]; then
    echo "Backup database error!"
    exit 1
fi
echo "Database:    " $BD/postgresql/${POSTGRES_DB:-wallet}.sql

# Backup Configs
mkdir -p $BD/conf
docker cp apc-wallet-api:${WALLET_API_CONF:-/wallet/conf/api.conf} $BD/conf/api.conf
docker cp apc-wallet-api:${GETH_API_CONF:-/wallet/conf/geth-api.conf} $BD/conf/geth-api.conf
if [ ! -f $BD/conf/api.conf ]; then
    echo "Backup configs (api.conf) error!"
    exit 1
fi
if [ ! -f $BD/conf/geth-api.conf ]; then
    echo "Backup configs (geth-api.conf) error!"
    exit 1
fi
echo "Сonfigs:     " $BD/conf

# Backup Env
cp .env $BD/.env
if [ ! -f $BD/.env ]; then
    echo "Backup .env error!"
    exit 1
fi
echo "Env:         " $BD/.env

# Backup Ethereum keystore
mkdir -p $BD/ethereum/testnet/
docker cp apc-ethereum:/root/.ethereum/keystore $BD/ethereum
docker cp apc-ethereum:/root/.ethereum/testnet/keystore $BD/ethereum/testnet
if [ ! -d $BD/ethereum/keystore ]; then
    echo "Backup Ethereum error!"
    exit 1
fi
echo "Ethereum:    " $BD/ethereum

# Backup Bitcoin Wallet
mkdir -p $BD/bitcoin
docker-compose exec -T bitcoin bitcoin-cli -rpcuser=${BITCOIN_RPC_USER:-rpcuser} -rpcpassword=${BITCOIN_RPC_PASSWORD:-rpcpassword} -rpcport=${BITCOIN_RPC_PORT:-8333/tcp} backupwallet /tmp/wallet.dat
docker cp apc-bitcoin:/tmp/wallet.dat $BD/bitcoin
docker-compose exec -T bitcoin rm /tmp/wallet.dat
if [ ! -f $BD/bitcoin/wallet.dat ]; then
    echo "Backup Bitcoin error!"
    exit 1
fi
echo "Bitcoin:     " $BD/bitcoin


# Backup Bitcoin Cash Wallet
mkdir -p $BD/bitcoin-cash
docker-compose exec -T bitcoin-cash bitcoin-cli -rpcuser=${BITCOIN_CASH_RPC_USER:-rpcuser} -rpcpassword=${BITCOIN_CASH_RPC_PASSWORD:-rpcpassword} -rpcport=${BITCOIN_CASH_RPC_PORT:-8332/tcp} backupwallet /tmp/wallet.dat
docker cp apc-bitcoin-cash:/tmp/wallet.dat $BD/bitcoin-cash
docker-compose exec -T bitcoin-cash rm /tmp/wallet.dat
if [ ! -f $BD/bitcoin-cash/wallet.dat ]; then
    echo "Backup Bitcoin Cash error!"
    exit 1
fi
echo "Bitcoin Cash:" $BD/bitcoin-cash

# Backup Litecoin
mkdir -p $BD/litecoin
docker-compose exec -T litecoin litecoin-cli -rpcuser=${LITECOIN_RPC_USER:-rpcuser} -rpcpassword=${LITECOIN_RPC_PASSWORD:-rpcpassword} -rpcport=${LITECOIN_RPC_PORT:-9332/tcp} backupwallet /tmp/wallet.dat
docker cp apc-litecoin:/tmp/wallet.dat $BD/litecoin
docker-compose exec -T litecoin rm /tmp/wallet.dat
if [ ! -f $BD/litecoin/wallet.dat ]; then
    echo "Backup Litecoin error!"
    exit 1
fi
echo "Litecoin:" $BD/litecoin

exit 0